/* NetID: knunes
   Name: Kaiden Nunes
   Section: 002
*/
#include <stdio.h>
#include "csapp.h"
#include <time.h>

/* Recommended max cache and object sizes */
#define MAX_CACHE_SIZE 1049000
#define MAX_OBJECT_SIZE 102400

#define BUFFER_SIZE 4096

#define NUMBER_OF_THREADS 5

#define CONNECTION_BUFFER_SIZE 100
#define LOGGING_BUFFER_SIZE 500
#define CACHE_SIZE 50
#define CACHE_CONTENT_SIZE 32768

#define MAX_LOG_MESSAGE_SIZE 4096

typedef struct
{
	int *buf;          /* Buffer array */
	int n;             /* Maximum number of slots */
	int front;         /* buf[(front+1)%n] is first item */
	int rear;          /* buf[rear%n] is last item */
	sem_t mutex;       /* Protects accesses to buf */
	sem_t slots;       /* Counts available slots */
	sem_t items;       /* Counts available items */
} connection_queue_t;

typedef struct
{
	char **message;     /* Buffer array */
	int n;             /* Maximum number of slots */
	int front;         /* buf[(front+1)%n] is first item */
	int rear;          /* buf[rear%n] is last item */
	sem_t mutex;       /* Protects accesses to buf */
	sem_t slots;       /* Counts available slots */
	sem_t items;       /* Counts available items */
} logging_queue_t;

typedef struct
{
	int response_size;
	char * response;

} http_response;

struct linked_list_node
{
	char *id;
	http_response response;
	struct linked_list_node *next;
};

typedef struct
{
	struct linked_list_node *nodes;
	struct linked_list_node *head;
	struct linked_list_node *tail;
	int n;
	int count;
	int max_size;
	int size;
	sem_t outer_read_write;
	sem_t reader_sem;
	sem_t reader_mutex;
	sem_t writer_sem;
	sem_t writer_mutex;
} linked_list_t;

/* You won't lose style points for including this long line in your code */
static const char *user_agent_hdr = "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:10.0.3) Gecko/20120305 Firefox/10.0.3";

connection_queue_t connection_buffer;
logging_queue_t logging_buffer;
linked_list_t cache;

int cache_read_count = 0;
int cache_write_count = 0;

void doit(int fd);
void read_requesthdrs(rio_t * rp);
int	parse_uri(char *uri, char *filename, char *cgiargs);
void connection_queue_init(connection_queue_t *queue, int n);
void connection_queue_deinit(connection_queue_t *queue);
void connection_queue_insert(connection_queue_t *queue, int item);
int connection_queue_remove(connection_queue_t *queue);
void logging_queue_init(logging_queue_t *queue, int n);
void logging_queue_deinit(logging_queue_t *queue);
void logging_queue_insert(logging_queue_t *queue, char *item);
char *logging_queue_remove(logging_queue_t *queue);
void insert_logging_request(char *message);
void linked_list_init(linked_list_t *list, int n, int max_size, int readers);
void linked_list_deinit(linked_list_t *list);
void linked_list_move(linked_list_t *list, char *id);
void linked_list_insert(linked_list_t *list, char *id, char *response, int response_size);
int linked_list_read(linked_list_t *list, char **cached_response, char *id);
void remove_characters(char *str, size_t n);
char * create_http_request(char * uri, char * port_string, char* base_uri_string);
int starts_with(const char *a, const char *b);
void serve_static(int fd, char *filename, int filesize);
void get_filetype(char *filename, char *filetype);
void serve_proxy_request(int fd, char * uri);
void serve_dynamic(int fd, char *filename, char *cgiargs);
void clienterror(int fd, char *cause, char *errnum, char *shortmsg, char *longmsg);
void *connection_thread(void *vargp);
void *logging_thread(void *vargp);
void clean_up_routine(int signal);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	socklen_t clientlen;
	struct sockaddr_storage clientaddr;
	pthread_t tid;

	Signal(SIGINT, clean_up_routine);

	listenfd = Open_listenfd(argv[1]);

	/* Initialize the connection and logging queues, as well as the linked list cache*/
	connection_queue_init(&connection_buffer, CONNECTION_BUFFER_SIZE);
	logging_queue_init(&logging_buffer, LOGGING_BUFFER_SIZE);
	linked_list_init(&cache, CACHE_SIZE, CACHE_CONTENT_SIZE, NUMBER_OF_THREADS);

	/* Create worker threads */
	for (unsigned int i = 0; i < NUMBER_OF_THREADS; i++)
	{
		Pthread_create(&tid, NULL, connection_thread, NULL);
	}
	/* Create logging thread */
	Pthread_create(&tid, NULL, logging_thread, NULL);

	while (1)
	{
		clientlen = sizeof(struct sockaddr_storage);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		/* Insert connfd in buffer */
		connection_queue_insert(&connection_buffer, connfd);
	}

}


/* Connection worker thread routine */
void *connection_thread(void *vargp)
{
	Pthread_detach(pthread_self());
	while (1)
	{
		int connfd = connection_queue_remove(&connection_buffer);
		doit(connfd);
		Close(connfd);
	}
}

/* Connection worker thread routine */
void *logging_thread(void *vargp)
{
	Pthread_detach(pthread_self());
	while (1)
	{
		char * log_message = logging_queue_remove(&logging_buffer);

		FILE *fp = fopen("events.log", "a");
		if (!fp)
		{
			printf("\n\nUnable to write to log file.");
		}

		/* Log the message */
		fprintf(fp, "%s\n", log_message);

		/* Close the file descriptor */
		fclose(fp);

		/* Free up the memory */
		Free(log_message);
	}
}

/*
* doit - handle one HTTP request/response transaction
*/
/* $begin doit */
void doit(int fd)
{
	int		is_static;
	struct stat	sbuf;
	char		buf[MAXLINE], method[MAXLINE], uri[MAXLINE], version[MAXLINE];
	char		filename[MAXLINE], cgiargs[MAXLINE];
	rio_t		rio;

	/* Read request line and headers */
	Rio_readinitb(&rio, fd);
	if (!Rio_readlineb(&rio, buf, MAXLINE))
		//line: netp: doit:readrequest
		return;
	printf("%s", buf);
	sscanf(buf, "%s %s %s", method, uri, version);

	/* Log the web request */
	insert_logging_request("Handling HTTP request");

	//line: netp: doit:parserequest
	if (strcasecmp(method, "GET"))
	{
		//line: netp: doit:beginrequesterr
		clienterror(fd, method, "501", "Not Implemented", "Tiny does not implement this method");
		return;
	} //line: netp: doit:endrequester
	read_requesthdrs(&rio);
	//line: netp: doit:readrequesthdrs

	/* Check to see if it is a proxy request*/
	if (strlen(uri) > 7 && starts_with(uri, "http://"))
	{
		serve_proxy_request(fd, uri);
		return;
	}

	/* Parse URI from GET request */
	is_static = parse_uri(uri, filename, cgiargs);
	//line: netp: doit:staticcheck
	if (stat(filename, &sbuf) < 0)
	{
		//line: netp: doit:beginnotfound
		clienterror(fd, filename, "404", "Not found", "Tiny couldn't find this file");
		return;
	} //line: netp: doit:endnotfound

	if (is_static)
	{
		/* Serve static content */
		if (!(S_ISREG(sbuf.st_mode)) || !(S_IRUSR & sbuf.st_mode))
		{
			//line: netp: doit:readable
			clienterror(fd, filename, "403", "Forbidden", "Tiny couldn't read the file");
			return;
		}
		serve_static(fd, filename, sbuf.st_size);
		//line: netp: doit:servestatic
	}
	else
	{
		/* Serve dynamic content */
		if (!(S_ISREG(sbuf.st_mode)) || !(S_IXUSR & sbuf.st_mode))
		{
			//line: netp: doit:executable
			clienterror(fd, filename, "403", "Forbidden",
				"Tiny couldn't run the CGI program");
			return;
		}
		serve_dynamic(fd, filename, cgiargs);
		//line: netp: doit:servedynamic
	}
}
/* $end doit */

void clean_up_routine(int signal)
{
	linked_list_deinit(&cache);
	connection_queue_deinit(&connection_buffer);
	logging_queue_deinit(&logging_buffer);
	exit(0);
}

/* Create an empty, bounded, shared FIFO buffer with n slots */
void connection_queue_init(connection_queue_t *queue, int n)
{
	queue->buf = Calloc(n, sizeof(int));
	queue->n = n;                    /* Buffer holds max of n items */
	queue->front = queue->rear = 0;     /* Empty buffer iff front == rear */
	Sem_init(&queue->mutex, 0, 1);   /* Binary semaphore for locking */
	Sem_init(&queue->slots, 0, n);   /* Initially, buf has n empty slots */
	Sem_init(&queue->items, 0, 0);   /* Initially, buf has 0 items */
}

/* Clean up buffer sp */
void connection_queue_deinit(connection_queue_t *queue)
{
	Free(queue->buf);
}

/* Remove and return the first item from buffer sp */
int connection_queue_remove(connection_queue_t *queue)
{
	int item;
	/* Wait for available item */
	P(&queue->items);

	/* Lock the buffer */
	P(&queue->mutex);

	/* Remove the item */
	int new_front = (queue->front + 1) % (queue->n);
	item = queue->buf[new_front];
	queue->front = new_front;

	/* Unlock the buffer */
	V(&queue->mutex);

	/* Announce available slot */
	V(&queue->slots);
	return item;
}

/* Insert item onto the rear of shared buffer sp */
void connection_queue_insert(connection_queue_t *queue, int item)
{
	/* Wait for available slot */
	P(&queue->slots);

	/* Lock the buffer */
	P(&queue->mutex);

	/* Insert the item */
	int new_rear = (queue->rear + 1) % (queue->n);
	queue->buf[new_rear] = item;
	queue->rear = new_rear;

	/* Unlock the buffer */
	V(&queue->mutex);

	/* Announce available item */
	V(&queue->items);
}


/* Create an empty, bounded, shared FIFO buffer with n slots */
void logging_queue_init(logging_queue_t *queue, int n)
{
	queue->message = Calloc(n, sizeof(char*));
	queue->n = n;                    /* Buffer holds max of n items */
	queue->front = queue->rear = 0;     /* Empty buffer iff front == rear */
	Sem_init(&queue->mutex, 0, 1);   /* Binary semaphore for locking */
	Sem_init(&queue->slots, 0, n);   /* Initially, buf has n empty slots */
	Sem_init(&queue->items, 0, 0);   /* Initially, buf has 0 items */
}

/* Clean up buffer sp */
void logging_queue_deinit(logging_queue_t *queue)
{
	Free(queue->message);
}

/* Remove and return the first item from buffer sp */
char *logging_queue_remove(logging_queue_t *queue)
{
	char *item;
	/* Wait for available item */
	P(&queue->items);

	/* Lock the buffer */
	P(&queue->mutex);

	/* Remove the item */
	int new_front = (queue->front + 1) % (queue->n);
	item = queue->message[new_front];
	queue->front = new_front;

	/* Unlock the buffer */
	V(&queue->mutex);

	/* Announce available slot */
	V(&queue->slots);
	return item;
}

/* Insert item onto the rear of shared buffer sp */
void logging_queue_insert(logging_queue_t *queue, char *item)
{
	/* Wait for available slot */
	P(&queue->slots);

	/* Lock the buffer */
	P(&queue->mutex);

	/* Insert the item */
	int new_rear = (queue->rear + 1) % (queue->n);
	queue->message[new_rear] = item;
	queue->rear = new_rear;

	/* Unlock the buffer */
	V(&queue->mutex);

	/* Announce available item */
	V(&queue->items);
}

/* Create an empty, bounded, shared linked list buffer with n slots */
void linked_list_init(linked_list_t *list, int n, int max_size, int readers)
{
	list->nodes = Calloc(n, sizeof(struct linked_list_node*));
	/* Buffer holds max of n items */
	list->n = n;
	list->count = 0;
	list->max_size = max_size;
	list->size = 0;
	list->head = list->tail = NULL;
	Sem_init(&list->outer_read_write, 0, 1);
	Sem_init(&list->reader_sem, 0, 1);
	Sem_init(&list->reader_mutex, 0, 1);
	Sem_init(&list->writer_sem, 0, 1);
	Sem_init(&list->writer_mutex, 0, 1);
}

/* Clean up buffer sp */
void linked_list_deinit(linked_list_t *list)
{
	/* Log that we are cleaning everything up */
	insert_logging_request("Cleaning up the linked list");

	if (list->head != NULL)
	{
		struct linked_list_node * current_node = list->head;
		do
		{
			struct linked_list_node * next_node = current_node->next;
			Free(current_node->id);
			Free(current_node->response.response);
			Free(current_node);
			current_node = next_node;
		} while (current_node != NULL);
	}
	Free(list->nodes);
}

/* Remove and return the first item from buffer sp */
int linked_list_read(linked_list_t *list, char** cached_response, char *id)
{
	P(&list->outer_read_write);
	P(&list->reader_sem);
	P(&list->reader_mutex);

	cache_read_count++;
	if (cache_read_count == 1)
	{
		P(&list->writer_sem);
	}

	V(&list->reader_mutex);
	V(&list->reader_sem);
	V(&list->outer_read_write);

	/* Log that we are looking for something in the cache */
	insert_logging_request("Looking for response to URI in cache");

	/* Start at the head */
	struct linked_list_node * current_node = list->head;

	/* Traverse the linked list */
	while (current_node != NULL)
	{
		http_response response = current_node->response;
		if (strcmp(id, current_node->id) == 0)
		{
			insert_logging_request("Found an item in the cache");

			/* Make an allocated copy of the response to give to the caller (this might not be necessary if we keep the reading and sending back the response to the client inside the reader's mutex) */
			*cached_response = (char *)Malloc(response.response_size);
			strncpy(*cached_response, response.response, response.response_size);

			P(&list->reader_mutex);
			cache_read_count--;
			if (cache_read_count == 0)
			{
				V(&list->writer_sem);
			}
			V(&list->reader_mutex);

			return response.response_size;
		}
		current_node = current_node->next;
	}

	P(&list->reader_mutex);
	cache_read_count--;
	if (cache_read_count == 0)
	{
		V(&list->writer_sem);
	}
	V(&list->reader_mutex);

	/* If you didn't return already, then nothing was found */
	return 0;
}

/* Insert item onto the rear of shared buffer sp */
void linked_list_move(linked_list_t *list, char *id)
{
	P(&list->writer_sem);
	cache_write_count++;
	if (cache_write_count == 1)
	{
		P(&list->reader_sem);
	}
	V(&list->writer_sem);
	P(&list->writer_mutex);

	/* Log that we are trying to move something out of the cache and into the front of the cache */
	insert_logging_request("Attempting to move response in cache to front of cache");

	/* Start at the head */
	struct linked_list_node * current_node = list->head;

	/* Keep track of the previous node */
	struct linked_list_node * previous_node = NULL;

	/* Traverse the linked list */
	while (current_node != NULL)
	{
		http_response response = current_node->response;

		/* When you find the node, delete it and insert it at the beginning */
		if (strcmp(id, response.response) == 0)
		{
			/* Only do something if the node being processed isn't the head */
			if (current_node != list->head)
			{
				/* Log that we found a match */
				insert_logging_request("Non-head match found, moving cache item to front of cache");

				/* Make the previous node point to whatever this node is pointing at */
				previous_node->next = current_node->next;

				/* If the node being deleted is the tail, then mark the previous node as the new tail */
				if (current_node == list->tail)
				{
					list->tail = previous_node;
				}

				/* Save the node ID to use to create the node again at the head of the list */
				char * node_id = current_node->id;

				/* Free up the current node (but not its ID nor the response string, because we will need those) */
				Free(current_node);

				/* Insert the response into the beginning of the cache */
				linked_list_insert(list, node_id, response.response, response.response_size);
			}

			V(&list->writer_mutex);
			P(&list->writer_sem);
			cache_write_count--;
			if (cache_write_count == 0)
			{
				V(&list->reader_sem);
			}
			V(&list->writer_sem);

			/* The cached response was found and moved, so quit */
			return;
		}

		/* Go to the next node, but keep track of the last node processed */
		previous_node = current_node;
		current_node = current_node->next;
	}

	V(&list->writer_mutex);
	P(&list->writer_sem);
	cache_write_count--;
	if (cache_write_count == 0)
	{
		V(&list->reader_sem);
	}
	V(&list->writer_sem);
}

void linked_list_insert(linked_list_t *list, char *id, char *response, int response_size)
{
	P(&list->writer_sem);
	cache_write_count++;
	if (cache_write_count == 1)
	{
		P(&list->reader_sem);
	}
	V(&list->writer_sem);
	P(&list->writer_mutex);

	/* Log that we are inserting something into the cache */
	insert_logging_request("Inserting response into the cache");

	/* If there isn't enough room in the last, delete the last element */
	while (list->count == list->n || list->size + response_size > list->max_size)
	{
		/* This means that there are no items in the cache, and that the item is just too big for the cache, then don't attempt to add the item, just skip it */
		if (list->count == 0)
		{
			/* Log that the item is too big for the cache */
			insert_logging_request("This item is too big for the cache, it will not be added to the cache");

			V(&list->writer_mutex);
			P(&list->writer_sem);
			cache_write_count--;
			if (cache_write_count == 0)
			{
				V(&list->reader_sem);
			}
			V(&list->writer_sem);
			return;
		}

		/* Log that we have filled out the entire cache size */
		insert_logging_request("The cache is full, deleting the tail");

		/* Get the current head */
		struct linked_list_node * current_node = list->head;

		/* If the node is both the head and the tail, then we need to mark the head as NULL (the content will be deleted by the tail pointer) */
		if (list->head == list->tail)
		{
			list->head = NULL;
		}
		else
		{
			/* Otherwise, find the node that points to the tail.*/
			while (strcmp(current_node->next->id, list->tail->id) != 0)
			{
				current_node = current_node->next;
			}
		}

		/* Mark how large the old tail was */
		unsigned int tail_size = list->tail->response.response_size;

		/* Make the current node point to wherever the old tail was pointing to */
		current_node->next = list->tail->next;

		/* Delete the old tail */
		Free(list->tail->id);
		Free(list->tail->response.response);
		Free(list->tail);

		/* Make the node that pointed to the old tail be the new tail */
		list->tail = current_node;

		/* Mark that we are removing from the cache */
		list->count -= 1;
		list->size -= tail_size;

		insert_logging_request("Deleted tail successfully");
	}

	/* Mark that we are adding to the cache */
	list->count += 1;
	list->size += response_size;

	/* Get the current head */
	struct linked_list_node * current_head = list->head;

	/* Create a new node which will be inserted before the head */
	struct linked_list_node * new_head = Malloc(sizeof(struct linked_list_node));

	new_head->id = id;
	new_head->response.response = response;
	new_head->response.response_size = response_size;

	/* Set that node to point to the old head */
	new_head->next = current_head;

	/* Set the new head as the head of the list */
	list->head = new_head;

	/* If the head is the only element in the list, then it is also the tail */
	if (list->tail == NULL)
	{
		list->tail = list->head;
	}

	V(&list->writer_mutex);
	P(&list->writer_sem);
	cache_write_count--;
	if (cache_write_count == 0)
	{
		V(&list->reader_sem);
	}
	V(&list->writer_sem);

	insert_logging_request("Added item to cache successfully");
}

void insert_logging_request(char *message)
{
	/* Get the current time */
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	/* Allocate memory to hold the log message */
	char * log_message = (char*)Malloc(MAX_LOG_MESSAGE_SIZE + 1);

	/* Put the message into the allocated memory */
	snprintf(log_message, MAX_LOG_MESSAGE_SIZE, "Event Log - Thread %ld\nMessage: %s.\n%s", pthread_self(), message, asctime(timeinfo));

	/* Log the message */
	logging_queue_insert(&logging_buffer, log_message);

}
void remove_characters(char *str, size_t n)
{
	size_t len = strlen(str);
	memmove(str, str + n, len - n + 1);
}

void serve_proxy_request(int fd, char * uri)
{
	/* See if the response is stored in the cache */
	char * cached_response = NULL;
	int cached_response_size = linked_list_read(&cache, &cached_response, uri);

	/* If we did find it, send the response back to the client */
	if (cached_response != NULL)
	{
		/* Log that we found something in the cache */
		insert_logging_request("Response to URI found in cache");

		/* Send the response back to the client */
		Write(fd, cached_response, cached_response_size);

		/* Free up the cached response copy that was retrieved */
		Free(cached_response);

		/* Move the item in the cache to the first position */
		linked_list_move(&cache, uri);

		/* We are done processing the request */
		return;
	}

	/* Log that we didn't find anything in the cache */
	insert_logging_request("Response to URI not found in cache, making the web request to get response");

	/* Save the full uri to put into the cache later */
	char *full_uri = (char *)Malloc(strlen(uri) + 1);
	strcpy(full_uri, uri);

	/* Remove the http:// part of the request */
	remove_characters(uri, 7);

	size_t len = strlen(uri);

	// Give enough room to hold a 12 bit port number
	char port_parsing_string[5];
	char base_uri[len];

	int parsing_base_uri = 1;
	unsigned int port_parsing_index = 0;
	unsigned int base_uri_index = 0;
	for (unsigned int i = 0; i < len; i++)
	{
		// If you found a port number, then you found the base uri and need to get the port
		if (uri[i] == ':')
		{
			parsing_base_uri = 0;
		}
		// If you found the end of the base string
		else if (uri[i] == '/')
		{
			break;
		}
		// Otherwise, add this to the base uri if we are still parsing that
		else if (parsing_base_uri)
		{
			base_uri[base_uri_index] = uri[i];
			base_uri_index++;
		}
		// Add it to the port string if we are parsing that
		else
		{
			port_parsing_string[port_parsing_index] = uri[i];
			port_parsing_index++;
		}
	}

	// Create string to hold the real base uri
	char base_uri_string[base_uri_index];
	for (unsigned int i = 0; i < base_uri_index; i++)
	{
		base_uri_string[i] = base_uri[i];
	}

	base_uri_string[base_uri_index] = '\0';

	char * port_string = NULL;
	if (port_parsing_index > 0)
	{
		port_string = Malloc(port_parsing_index + 1);
		for (unsigned int i = 0; i < port_parsing_index; i++)
		{
			port_string[i] = port_parsing_string[i];
		}
		port_string[port_parsing_index] = '\0';
	}
	else
	{
		port_string = Malloc(3);
		port_string[0] = '8';
		port_string[1] = '0';
		port_string[2] = '\0';
	}

	unsigned int char_to_remove = base_uri_index;
	if (port_parsing_index > 0)
	{
		char_to_remove += port_parsing_index + 1;
	}

	// What is left is the URI minus the base uri and port
	remove_characters(uri, char_to_remove);

	if (strlen(uri) == 0)
	{
		uri = "/";
	}

	/* If the response isn't stored in the cache, then create the http request string */
	char http_request[MAXBUF];
	sprintf(http_request, "GET %s HTTP/1.0\r\n", uri);
	sprintf(http_request, "%sHost: %s\r\n", http_request, base_uri_string);
	sprintf(http_request, "%sUser-Agent: %s\r\n", http_request, user_agent_hdr);
	sprintf(http_request, "%sConnection: close\r\n", http_request);
	sprintf(http_request, "%sProxy-Connection: close\r\n\r\n", http_request);

	/* Get the server file descriptor */
	int server_fd = open_clientfd(base_uri_string, port_string);

	/* Make sure you had a successful opening */
	if (server_fd == -2)
	{
		clienterror(fd, "Finding server info", "400", "Bad Request", "No server exists with that url");
		return;
	}
	else if (server_fd == -1)
	{
		clienterror(fd, "Opening socket", "500", "Server Error", "We couldn't connect with the server");
		return;
	}
	/* Free up the port string we allocated */
	Free(port_string);

	/* Make the request to the server */
	Write(server_fd, http_request, strlen(http_request));

	/* Get the response from the server */
	unsigned int response_size_total = 0;
	unsigned int response_size = 0;

	char * buffer = Malloc(BUFFER_SIZE);
	char * response = NULL;
	while ((response_size = Read(server_fd, buffer, BUFFER_SIZE)) != 0)
	{
		if (response == NULL)
		{
			response = Malloc(response_size);
		}
		else
		{
			response = Realloc(response, response_size_total + response_size);
		}
		memcpy(response + response_size_total, buffer, response_size);
		response_size_total += response_size;
	}

	/* Free up the buffer space */
	Free(buffer);

	/* Close the server socket */
	Close(server_fd);

	/* Send the response back to the client */
	Write(fd, response, response_size_total);

	/* Add the response to the cache (no need to free it, since we will just keep it in the cache) */
	linked_list_insert(&cache, full_uri, response, response_size_total);
}

/* Checks to see if a string starts with some substring */
int starts_with(const char *a, const char *b)
{
	if (strncmp(a, b, strlen(b)) == 0) return 1;
	return 0;
}

/*
* read_requesthdrs - read HTTP request headers
*/
/* $begin read_requesthdrs */
void read_requesthdrs(rio_t * rp)
{
	char		buf[MAXLINE];

	Rio_readlineb(rp, buf, MAXLINE);
	printf("%s", buf);
	while (strcmp(buf, "\r\n"))
	{
		//line: netp: readhdrs:checkterm
		Rio_readlineb(rp, buf, MAXLINE);
		printf("%s", buf);
	}
	return;
}
/* $end read_requesthdrs */

/*
* parse_uri - parse URI into filename and CGI args return 0 if dynamic
* content, 1 if static
*/
/* $begin parse_uri */
int parse_uri(char *uri, char *filename, char *cgiargs)
{
	char *ptr;

	if (!strstr(uri, "cgi-bin"))
	{
		printf("\nTesting this uri: %s", uri);
		/* Static content */
		//line: netp: parseuri:isstatic
		strcpy(cgiargs, "");
		//line: netp: parseuri:clearcgi
		strcpy(filename, ".");
		//line: netp: parseuri:beginconvert1
		strcat(filename, uri);
		//line: netp: parseuri:endconvert1
		if (uri[strlen(uri) - 1] == '/')
			//line: netp: parseuri:slashcheck
			strcat(filename, "home.html");
		//line: netp: parseuri:appenddefault
		return 1;
	}
	else
	{
		/* Dynamic content */
		//line: netp: parseuri:isdynamic
		ptr = index(uri, '?');
		//line: netp: parseuri:beginextract
		if (ptr)
		{
			strcpy(cgiargs, ptr + 1);
			*ptr = '\0';
		}
		else
			strcpy(cgiargs, "");
		//line: netp: parseuri:endextract
		strcpy(filename, ".");
		//line: netp: parseuri:beginconvert2
		strcat(filename, uri);
		//line: netp: parseuri:endconvert2
		return 0;
	}
}
/* $end parse_uri */

/*
* serve_static - copy a file back to the client
*/
/* $begin serve_static */
void serve_static(int fd, char *filename, int filesize)
{
	int		srcfd;
	char           *srcp, filetype[MAXLINE], buf[MAXBUF];

	/* Send response headers to client */
	get_filetype(filename, filetype);
	//line: netp: servestatic:getfiletype
	sprintf(buf, "HTTP/1.0 200 OK\r\n");
	//line: netp: servestatic:beginserve
	sprintf(buf, "%sServer: Tiny Web Server\r\n", buf);
	sprintf(buf, "%sConnection: close\r\n", buf);
	sprintf(buf, "%sContent-length: %d\r\n", buf, filesize);
	sprintf(buf, "%sContent-type: %s\r\n\r\n", buf, filetype);
	Rio_writen(fd, buf, strlen(buf));
	//line: netp: servestatic:endserve
	printf("Response headers:\n");
	printf("%s", buf);

	/* Send response body to client */
	srcfd = Open(filename, O_RDONLY, 0);
	//line: netp: servestatic:open
	srcp = Mmap(0, filesize, PROT_READ, MAP_PRIVATE, srcfd, 0);
	//line: netp: servestatic:mmap
	Close(srcfd);
	//line: netp: servestatic:close
	Rio_writen(fd, srcp, filesize);
	//line: netp: servestatic:write
	Munmap(srcp, filesize);
	//line: netp: servestatic:munmap
}

/*
* get_filetype - derive file type from file name
*/
void get_filetype(char *filename, char *filetype)
{
	if (strstr(filename, ".html"))
		strcpy(filetype, "text/html");
	else if (strstr(filename, ".gif"))
		strcpy(filetype, "image/gif");
	else if (strstr(filename, ".png"))
		strcpy(filetype, "image/png");
	else if (strstr(filename, ".jpg"))
		strcpy(filetype, "image/jpeg");
	else
		strcpy(filetype, "text/plain");
}
/* $end serve_static */

/*
* serve_dynamic - run a CGI program on behalf of the client
*/
/* $begin serve_dynamic */
void serve_dynamic(int fd, char *filename, char *cgiargs)
{
	char		buf[MAXLINE], *emptylist[] = { NULL };

	/* Return first part of HTTP response */
	sprintf(buf, "HTTP/1.0 200 OK\r\n");
	Rio_writen(fd, buf, strlen(buf));
	sprintf(buf, "Server: Tiny Web Server\r\n");
	Rio_writen(fd, buf, strlen(buf));

	if (Fork() == 0)
	{			/* Child */
				//line: netp: servedynamic:fork
				/* Real server would set all CGI vars here */
		setenv("QUERY_STRING", cgiargs, 1);
		//line: netp: servedynamic:setenv
		Dup2(fd, STDOUT_FILENO);	/* Redirect stdout to
									* client */
									//line: netp: servedynamic:dup2
		Execve(filename, emptylist, environ);	/* Run CGI program */
												//line: netp: servedynamic:execve
	}
	Wait(NULL);		/* Parent waits for and reaps child */
					//line: netp: servedynamic:wait
}
/* $end serve_dynamic */

/*
* clienterror - returns an error message to the client
*/
/* $begin clienterror */
void clienterror(int fd, char *cause, char *errnum,
	char *shortmsg, char *longmsg)
{
	char		buf[MAXLINE], body[MAXBUF];

	/* Build the HTTP response body */
	sprintf(body, "<html><title>Tiny Error</title>");
	sprintf(body, "%s<body bgcolor=" "ffffff" ">\r\n", body);
	sprintf(body, "%s%s: %s\r\n", body, errnum, shortmsg);
	sprintf(body, "%s<p>%s: %s\r\n", body, longmsg, cause);
	sprintf(body, "%s<hr><em>The Tiny Web server</em>\r\n", body);

	/* Print the HTTP response */
	sprintf(buf, "HTTP/1.0 %s %s\r\n", errnum, shortmsg);
	Rio_writen(fd, buf, strlen(buf));
	sprintf(buf, "Content-type: text/html\r\n");
	Rio_writen(fd, buf, strlen(buf));
	sprintf(buf, "Content-length: %d\r\n\r\n", (int)strlen(body));
	Rio_writen(fd, buf, strlen(buf));
	Rio_writen(fd, body, strlen(body));
}
/* $end clienterror */
